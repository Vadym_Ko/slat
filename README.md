  Simple data managing tool. 
  
  Program based on Spring Boot + Vaadin + PostgreSQL database. 
  Uses to manage any data (in this example it deals with books). It can save, 
update, delete and sort data by any field (in this example by Title, Author, 
Year, Genre, Bookcase Number). All data stores in PostgreSQL database, so it must 
be installed and configured to localhost:5432 with clear "books" and "test" 
databases. If you want to store data in another database you need to change 
settings in "main/resources/application.properties" and 
"test/resources/application.properties". The program uses "books" database for 
store data, and "test" database only for tests. After running you must link 
by your web browser to http://localhost:8080.
    
  You can edit and delete data by clicking on it in the grid. Of course, for first
you must create some data fields. 
  