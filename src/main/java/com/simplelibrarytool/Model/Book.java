package com.simplelibrarytool.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Describes books for the library
 */

@Data
@Entity
public class Book {

    @Id
    @GeneratedValue
    private long id;
    private String title;
    private String author;
    private String year;
    private String genre;
    private String description;
    private String bookcaseNumber;

    public Book(){

    }

    /**
     * Constructor
     * @param title Title of the book
     * @param author Author of the book
     * @param year Year of the book
     * @param genre Genre of the book
     * @param description The book's description
     * @param bookcaseNumber Marker of the place where book stores
     */

    public Book(String title, String author, String year, String genre, String description, String bookcaseNumber) {
        this.title = title;
        this.author = author;
        this.year = year;
        this.genre = genre;
        this.description = description;
        this.bookcaseNumber = bookcaseNumber;
    }

}
