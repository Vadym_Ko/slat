package com.simplelibrarytool.UI;

import com.simplelibrarytool.DAO.BookRepository;
import com.simplelibrarytool.Model.Book;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

/**
 *   Simple data managing tool.
 *   see Readme.md fo details
 */

@Route
public class MainView extends VerticalLayout {

    protected final BookRepository data;
    private final BookEditor editor;
    private final H2 header = new H2("Simple library administrative tool");
    private final TextField titleFilter = new TextField();
    private final TextField authorFilter = new TextField();
    private final TextField yearFilter = new TextField();
    private final TextField genreFilter = new TextField();
    private final TextField bookcaseNumberFilter = new TextField();
    private final Label gridTitle = new Label("List of existing books");
    public Grid<Book> grid = new Grid<>(Book.class);
    private final Button newBut = new Button("New Book");

    /**
     * Constructor
     * @param data object of  JpaRepository type
     * @param editor object from this package
     */
    public MainView(BookRepository data, BookEditor editor) {
        this.data = data;
        this.editor = editor;
        //this.titleFilter = new TextField();
        //this.authorFilter = new TextField();
        //this.yearFilter = new TextField();
        //this.genreFilter = new TextField();
        //this.bookcaseNumberFilter = new TextField();
        //this.grid = new Grid<>(Book.class);
        //this.newBut = new Button("New Book");

        configureUi();
        buildUi();
        installListeners();
    }

    private void configureUi() {
        grid.setHeight("300px");
        grid.setColumns("id", "title", "author", "year", "genre", "description", "bookcaseNumber");
        grid.getColumnByKey("id").setFlexGrow(0).setWidth("100px").setResizable(false);
        grid.getColumnByKey("year").setFlexGrow(0).setWidth("100px").setResizable(false);
        grid.getColumnByKey("bookcaseNumber").setFlexGrow(0).setWidth("150px").setResizable(false);
        grid.setItems(data.findAll());

        titleFilter.setWidth("300px");
        bookcaseNumberFilter.setWidth("250px");
        titleFilter.setPlaceholder("Sort by title");
        authorFilter.setPlaceholder("Sort by author");
        yearFilter.setPlaceholder("Sort by year");
        genreFilter.setPlaceholder("Sort by genre");
        bookcaseNumberFilter.setPlaceholder("Sort by Bookcase Number");

        editor.setVisible(false);
    }
     private void buildUi() {
        add(header);
        HorizontalLayout filterLayout = new HorizontalLayout(titleFilter, authorFilter,
                yearFilter, genreFilter, bookcaseNumberFilter);
        add(filterLayout, gridTitle, grid, newBut, editor);
    }

    private void installListeners() {

        titleFilter.setValueChangeMode(ValueChangeMode.EAGER);
        titleFilter.addValueChangeListener(event -> grid.setItems(
                data.findByTitleStartsWithIgnoreCase(event.getValue())
        ));
        authorFilter.setValueChangeMode(ValueChangeMode.EAGER);
        authorFilter.addValueChangeListener(event -> grid.setItems(
                data.findByAuthorStartsWithIgnoreCase(event.getValue())
        ));
        yearFilter.setValueChangeMode(ValueChangeMode.EAGER);
        yearFilter.addValueChangeListener(event -> grid.setItems(
                data.findByYearStartsWithIgnoreCase(event.getValue())
        ));
        genreFilter.setValueChangeMode(ValueChangeMode.EAGER);
        genreFilter.addValueChangeListener(event -> grid.setItems(
                data.findByGenreStartsWithIgnoreCase(event.getValue())
        ));
        bookcaseNumberFilter.setValueChangeMode(ValueChangeMode.EAGER);
        bookcaseNumberFilter.addValueChangeListener(event -> grid.setItems(
                data.findByBookcaseNumberStartsWithIgnoreCase(event.getValue())
        ));

        grid.asSingleSelect().addValueChangeListener(event -> {
            editor.editBook(event.getValue());
        });

        newBut.addClickListener(event -> {
            editor.editBook(new Book("","","","","",""));
        });

        editor.anyEditorButtonPushed(() -> {
            editor.setVisible(false);
            grid.setItems(data.findByTitleStartsWithIgnoreCase(titleFilter.getValue()));
        });
    }
}

