package com.simplelibrarytool.UI;

import com.simplelibrarytool.DAO.BookRepository;
import com.simplelibrarytool.Model.Book;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *  Edit, save or delete incoming books
 */
@SpringComponent
@UIScope
public class BookEditor extends VerticalLayout implements KeyNotifier {
    private final BookRepository data;

    private Book book;

    public final TextField title = new TextField("Title");
    public final TextField author = new TextField("Author");
    public final TextField year = new TextField("Year");
    public final TextField genre = new TextField("Genre");
    public final TextField description = new TextField("Description");
    public final TextField bookcaseNumber = new TextField("Bookcase Number");

    private final Button save = new Button("Save");
    private final Button cancel = new Button("Cancel");
    private final Button delete = new Button("Delete");

    private final Binder<Book> binder = new Binder<>(Book.class);

    private ChangeHandler changeHandler;

    /**
     * Constructor
     * @param data object of JpaRepository type
     */
    @Autowired
    public BookEditor(BookRepository data)  {
        this.data = data;

        buildBookEditorUi();

        binder.bindInstanceFields(this);

        installActionButtonsListeners();
    }

    private void buildBookEditorUi() {
        HorizontalLayout textLayout = new HorizontalLayout(title,author,year,genre,description,bookcaseNumber);
        HorizontalLayout actButtons = new HorizontalLayout(save,cancel,delete);
        add(textLayout);
        add(actButtons);
        setSpacing(true);
    }

    private void installActionButtonsListeners() {
        save.addClickListener(event -> saveBook());
        cancel.addClickListener(event -> cancel());
        delete.addClickListener(event -> deleteBook());
        setVisible(false);
    }

    /**
     *  This interface allows to handle that button is pushed
     */
    public interface ChangeHandler {
        void onChange();
    }

    /**
     * Save book to DB
     */
    public void saveBook() {
        data.save(book);
        changeHandler.onChange();
    }

    /**
     * Delete book from DB
     */
    public void deleteBook(){
        data.delete(book);
        changeHandler.onChange();
    }

    /**
     * Cancel book editing
     */
    public void cancel(){
        changeHandler.onChange();
    }

    /**
     * Book editor uses for editing book, it can be a new book or existing book
     * @param bookA book for edit
     */
    public void editBook (Book bookA) {

        boolean isBookNull = bookA == null;
        if (isBookNull) {
            setVisible(false);
            return;
        }
        boolean isItBookForEdit = bookA.getId() != 0;
        if (isItBookForEdit) {
            delete.setVisible(true);
            book = data.findById(bookA.getId()).get();

        }
        else {
            delete.setVisible(false);
            book = bookA;

        }
        binder.setBean(book);
        setVisible(true);
        title.focus();
    }

    /**
     * Notified when action buttons is clicked
     * @param h event
     */
    public void anyEditorButtonPushed(ChangeHandler h){
        changeHandler = h;
    }


}

