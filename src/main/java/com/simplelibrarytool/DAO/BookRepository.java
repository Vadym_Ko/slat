package com.simplelibrarytool.DAO;

import com.simplelibrarytool.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book,Long> {

     List<Book> findByTitleStartsWithIgnoreCase (String title);
     List<Book> findByAuthorStartsWithIgnoreCase (String author);
     List<Book> findByYearStartsWithIgnoreCase (String year);
     List<Book> findByGenreStartsWithIgnoreCase (String genre);
     List<Book> findByBookcaseNumberStartsWithIgnoreCase (String genre);

}
