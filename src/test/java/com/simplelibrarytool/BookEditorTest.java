package com.simplelibrarytool;

import com.simplelibrarytool.DAO.BookRepository;
import com.simplelibrarytool.Model.Book;
import com.simplelibrarytool.UI.BookEditor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.then;


@RunWith(MockitoJUnitRunner.class)
public class BookEditorTest {

    private static final String TITLE = "Title";
    private static final String AUTHOR = "Author";
    private static final String YEAR = "Year";
    private static final String GENRE = "Genre";
    private static final String DESCRIPTION = "Description";
    private static final String BOOKCASE_NUMBER = "Bookcase Number";


    @Mock
    private BookRepository data;
    @InjectMocks
    private BookEditor editor;
    @Mock
    private BookEditor.ChangeHandler changeHandler;

    @Before
    public void init() {
        data.deleteAll();
        editor.anyEditorButtonPushed(changeHandler);
    }

    @Test
    public void shouldStoreCustomerInRepoWhenEditorSaveClicked() {
        emptyCustomerWasSetToForm();

        this.editor.title.setValue(TITLE);
        this.editor.author.setValue(AUTHOR);
        this.editor.year.setValue(YEAR);
        this.editor.genre.setValue(GENRE);
        this.editor.description.setValue(DESCRIPTION);
        this.editor.bookcaseNumber.setValue(BOOKCASE_NUMBER);

        this.editor.saveBook();

        then(this.data).should().save(argThat(customerMatchesEditorFields()));
    }

    @Test
    public void shouldDeleteCustomerFromRepoWhenEditorDeleteClicked() {
        customerDataWasFilled();

        editor.deleteBook();

        then(this.data).should().delete(argThat(customerMatchesEditorFields()));
    }

    private void emptyCustomerWasSetToForm() {
        this.editor.editBook(new Book());
    }
    private void customerDataWasFilled() {
        this.editor.editBook(new Book(TITLE,AUTHOR,YEAR,GENRE,DESCRIPTION, BOOKCASE_NUMBER));
    }

    private ArgumentMatcher<Book> customerMatchesEditorFields() {
        return book -> TITLE.equals(book.getTitle()) && AUTHOR.equals(book.getAuthor())
                && YEAR.equals(book.getYear()) && GENRE.equals(book.getGenre())
                && DESCRIPTION.equals(book.getDescription()) && BOOKCASE_NUMBER.equals(book.getBookcaseNumber());
    }

}
