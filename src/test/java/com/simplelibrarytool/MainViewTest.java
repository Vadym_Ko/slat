package com.simplelibrarytool;
import com.simplelibrarytool.DAO.BookRepository;
import com.simplelibrarytool.Model.Book;
import com.simplelibrarytool.UI.BookEditor;
import com.simplelibrarytool.UI.MainView;
import com.vaadin.flow.data.provider.ListDataProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MainViewTest {

    private MainView mainView;

    @Autowired
    BookRepository data;

    private BookEditor editor;

    @Before
    public void setup() {

       this.editor = new BookEditor(this.data);
       this.mainView = new MainView(this.data, editor);
    }


    @Test
    public void shouldInitializeTheGridWithCustomerRepositoryData() {
        int customerCount = (int) this.data.count();

        then(mainView.grid.getColumns()).hasSize(7);
        then(getCustomersInGrid()).hasSize(customerCount);
    }

    private List<Book> getCustomersInGrid() {
        ListDataProvider<Book> ldp = (ListDataProvider) mainView.grid.getDataProvider();
        return new ArrayList<>(ldp.getItems());
    }

    @Test
    public void shouldFillOutTheGridWithNewData() {
        int initialBookCount = (int) this.data.count();

        customerDataWasFilled(editor,"Title3","Author3","2019","Genre3",
                "Description3","Bkcase3");

        this.editor.saveBook();

        then(getCustomersInGrid()).hasSize(initialBookCount + 1);

        then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
                .extracting("title","author","year","genre","description","bookcaseNumber")
                .containsExactly("Title3","Author3","2019","Genre3", "Description3","Bkcase3");
    }

    @Test
    public void shouldFilterOutTheGridWithTheProvidedName() {

        mainView.grid.setItems(data.findByAuthorStartsWithIgnoreCase("Author2"));

        then(getCustomersInGrid()).hasSize(1);
        then(getCustomersInGrid().get(getCustomersInGrid().size() - 1))
                .extracting("title","author","year","genre","description","bookcaseNumber")
                .containsExactly("Title2","Author2","2019","Genre2","Description2","Bkcase2");
    }

    @Test
    public void shouldInitializeWithInvisibleEditor() {

        then(this.editor.isVisible()).isFalse();
    }

    @Test
    public void shouldMakeEditorVisible() {
        Book first = getCustomersInGrid().get(0);
        this.mainView.grid.select(first);

        then(this.editor.isVisible()).isTrue();
    }

    private void customerDataWasFilled(BookEditor editor, String title, String author,
                                       String year, String genre, String description,
                                       String bookcaseNumber) {
        this.editor.title.setValue(title);
        this.editor.author.setValue(author);
        this.editor.year.setValue(year);
        this.editor.genre.setValue(genre);
        this.editor.description.setValue(description);
        this.editor.bookcaseNumber.setValue(bookcaseNumber);
        editor.editBook(new Book(title,author,year,genre,description,bookcaseNumber));
    }



    @Configuration
    @EnableAutoConfiguration
    static class Config {

        @Autowired
        BookRepository data;

        @PostConstruct
        public void initializeData() {
            this.data.deleteAll();
            this.data.save(new Book("Title1","Author1","2019","Genre1",
                    "Description1","Bkcase1"));
            this.data.save(new Book("Title2","Author2","2019","Genre2",
                    "Description2","Bkcase2"));
        }
    }
}